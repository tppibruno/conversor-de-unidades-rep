import java.util.Scanner;
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* declara as variaveis incluindo um scanner para poder ler as opcoes
		 * dadas a partir do teclado pelo utilizador
		 */
		Scanner sc = new Scanner ( System . in );
		String opcao ;
		/* imprime o menu */
		System . out . println (" Conversor de unidades ");
		System . out . println ("a) m -> km");
		System . out . println ("b) m -> cm");
		System . out .println("c) Inv�lido");

		/* le a opcao do utilizador */
		opcao = sc . next () ;

		/* faz a conversao de acordo com a opcao */
		if ( opcao . equals ("a") ){
			System . out . println ("m = ");
			float m = sc . nextInt () ;
			System . out . println (m + " m = " + m / 1000.0 + " km");
		} else if ( opcao . equals ("b")){
			/* ... */
			float m = sc . nextInt () ;
			System.out.println(m * 60);
		} else {
			System . out . println (" Opcao invalida ");
		}

		sc . close () ;
	}

}
